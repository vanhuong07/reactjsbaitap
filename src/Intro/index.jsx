import React, { Component } from "react";
import './index.css';
// const data = [
//   {
//     id: "sp_1",
//     name: "iphoneX",
//     price: "30.000.000 VNÄ�",
//     screen: "screen_1",
//     backCamera: "backCamera_1",
//     frontCamera: "frontCamera_1",
//     img:
//       "https://sudospaces.com/mobilecity-vn/images/2019/12/iphonex-black.jpg",
//     desc:
//       "iPhone X features a new all-screen design. Face ID, which makes your face your password",
//   },
//   {
//     id: "sp_2",
//     name: "Note 7",
//     price: "20.000.000 VNÄ�",
//     screen: "screen_2",
//     backCamera: "backCamera_2",
//     frontCamera: "frontCamera_2",
//     img:
//       "https://www.xtmobile.vn/vnt_upload/product/01_2018/thumbs/600_note_7_blue_600x600.png",
//     desc:
//       "The Galaxy Note7 comes with a perfectly symmetrical design for good reason",
//   },
//   {
//     id: "sp_3",
//     name: "Vivo",
//     price: "10.000.000 VNÄ�",
//     screen: "screen_3",
//     backCamera: "backCamera_3",
//     frontCamera: "frontCamera_3",
//     img: "https://www.gizmochina.com/wp-content/uploads/2019/11/Vivo-Y19.jpg",
//     desc:
//       "A young global smartphone brand focusing on introducing perfect sound quality",
//   },
//   {
//     id: "sp_4",
//     name: "Blacberry",
//     price: "15.000.000 VNÄ�",
//     screen: "screen_4",
//     backCamera: "backCamera_4",
//     frontCamera: "frontCamera_4",
//     img:
//       "https://cdn.tgdd.vn/Products/Images/42/194834/blackberry-keyone-3gb-600x600.jpg",
//     desc:
//       "BlackBerry is a line of smartphones, tablets, and services originally designed",
//   },
// ];
class Intro extends Component {
    state ={
        productsList:  [
            {
              id: "sp_1",
              name: "iphoneX",
              price: "30.000.000 VNÄ�",
              screen: "screen_1",
              backCamera: "backCamera_1",
              frontCamera: "frontCamera_1",
              img:
                "https://sudospaces.com/mobilecity-vn/images/2019/12/iphonex-black.jpg",
              desc:
                "iPhone X features a new all-screen design. Face ID, which makes your face your password",
            },
            {
              id: "sp_2",
              name: "Note 7",
              price: "20.000.000 VNÄ�",
              screen: "screen_2",
              backCamera: "backCamera_2",
              frontCamera: "frontCamera_2",
              img:
                "https://www.xtmobile.vn/vnt_upload/product/01_2018/thumbs/600_note_7_blue_600x600.png",
              desc:
                "The Galaxy Note7 comes with a perfectly symmetrical design for good reason",
            },
            {
              id: "sp_3",
              name: "Vivo",
              price: "10.000.000 VNÄ�",
              screen: "screen_3",
              backCamera: "backCamera_3",
              frontCamera: "frontCamera_3",
              img: "https://www.gizmochina.com/wp-content/uploads/2019/11/Vivo-Y19.jpg",
              desc:
                "A young global smartphone brand focusing on introducing perfect sound quality",
            },
            {
              id: "sp_4",
              name: "Blacberry",
              price: "15.000.000 VNÄ�",
              screen: "screen_4",
              backCamera: "backCamera_4",
              frontCamera: "frontCamera_4",
              img:
                "https://cdn.tgdd.vn/Products/Images/42/194834/blackberry-keyone-3gb-600x600.jpg",
              desc:
                "BlackBerry is a line of smartphones, tablets, and services originally designed",
            },
          ]
    }
    show_products = () => {
        const products = this.state.productsList.map((item,index) => 
        <div className="col-3" key={index}>
            <div className="card">
                <img src={item.img} alt="iphone" />
                <div className="card-body">
                <h3>{item.name}</h3>
                <p>{item.desc}</p>
                </div>
            </div>
        </div>);
        console.log(this.state.productsList);
        return products;
    }
  render() {
    return (
            <div className="container">
                <h3>BEST SMARTPHONE</h3>
                <div className="row">
                {this.show_products()}
                </div>
                {/* <div className="row">
                    {data.map((item,index) =>{
                        return (
                            <div className="col-3" key={index}>
                                <div className="card">
                                    <img src={item.img} alt="iphone" />
                                    <div className="card-body">
                                        <h3>{item.name}</h3>
                                        <p>{item.desc}</p>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div> */}

        
            </div>
    );;
  }
}

export default Intro;
