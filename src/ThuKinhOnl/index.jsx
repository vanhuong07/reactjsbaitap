import React, { Component } from "react";
import background from "../image/background.jpg";
import model from "../image/model.jpg";
import classes from "./style.module.css";
import g1 from "../image/g1.jpg";
import g2 from "../image/g2.jpg";
import g3 from "../image/g3.jpg";
import g4 from "../image/g4.jpg";
import g5 from "../image/g5.jpg";
import g6 from "../image/g6.jpg";
import g7 from "../image/g7.jpg";
import g8 from "../image/g8.jpg";
import g9 from "../image/g9.jpg";
import v1 from "../image/v1.png";
import v2 from "../image/v2.png";
import v3 from "../image/v3.png";
import v4 from "../image/v4.png";
import v5 from "../image/v5.png";
import v6 from "../image/v6.png";
import v7 from "../image/v7.png";
import v8 from "../image/v8.png";
import v9 from "../image/v9.png";


class ThuKinh extends Component {
  state = {
    KinhImg: v1,
  };
//   arrProduct = [
//     {
//       id: 1,
//       price: 30,
//       name: "GUCCI G8850U",
//       url: "./glassesImage/v1.png",
//       desc:
//         "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
//     },
//     {
//       id: 2,
//       price: 50,
//       name: "GUCCI G8759H",
//       url: "./glassesImage/v2.png",
//       desc:
//         "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
//     },
//     {
//       id: 3,
//       price: 30,
//       name: "DIOR D6700HQ",
//       url: "./glassesImage/v3.png",
//       desc:
//         "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
//     },
//     {
//       id: 4,
//       price: 30,
//       name: "DIOR D6005U",
//       url: "./glassesImage/v4.png",
//       desc:
//         "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
//     },
//     {
//       id: 5,
//       price: 30,
//       name: "PRADA P8750",
//       url: "./glassesImage/v5.png",
//       desc:
//         "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
//     },
//     {
//       id: 6,
//       price: 30,
//       name: "PRADA P9700",
//       url: "./glassesImage/v6.png",
//       desc:
//         "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
//     },
//     {
//       id: 7,
//       price: 30,
//       name: "FENDI F8750",
//       url: "./glassesImage/v7.png",
//       desc:
//         "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
//     },
//     {
//       id: 8,
//       price: 30,
//       name: "FENDI F8500",
//       url: "./glassesImage/v8.png",
//       desc:
//         "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
//     },
//     {
//       id: 9,
//       price: 30,
//       name: "FENDI F4300",
//       url: "./glassesImage/v9.png",
//       desc:
//         "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
//     },
//   ];
  changeColor = (img) => () => {
    this.setState(
      {
        KinhImg: img,
      },
      () => {
        console.log(this.state.KinhImg);
      }
    );
  };
//   renderDetail = () => {
//       return this.arrProduct.map((item,index) =>{
//           return (
//               <div className={classes.text} key={index}>
//                   <h3>{item.name}</h3>
//                   <p>{item.desc}</p>
//               </div>
//           )
//       })
//   }
  render() {
    return (
      <div>
        <img src={background} alt="bg" className={classes.bgImg} />
        <div className="container">
          <div className={classes.imgtest}>
            <div className={classes.imgitem}>
              <img src={model} alt="anh" className={classes.imgtest} />
              {/* <div className={classes.boxtext}>
                    {this.renderDetails()}
              </div> */}
            </div>
            <div className={classes.imgitem1}>
              <img src={model} alt="anh" className={classes.imgtest} />
            </div>
            <div className={classes.img}>
              <img
                src={this.state.KinhImg}
                alt="kinh"
                className={classes.kinh}
              />
            </div>
            <div className={classes.buttonimg}>
              <button
                className={classes.buttonitem}
                onClick={this.changeColor(v1)}
              >
                <img src={g1} alt="v1button" className={classes.imgbut} />
              </button>
              <button
                className={classes.buttonitem}
                onClick={this.changeColor(v2)}
              >
                <img src={g2} alt="v1button" className={classes.imgbut} />
              </button>
              <button
                className={classes.buttonitem}
                onClick={this.changeColor(v3)}
              >
                <img src={g3} alt="v1button" className={classes.imgbut} />
              </button>
              <button
                className={classes.buttonitem}
                onClick={this.changeColor(v4)}
              >
                <img src={g4} alt="v1button" className={classes.imgbut} />
              </button>
              <button
                className={classes.buttonitem}
                onClick={this.changeColor(v5)}
              >
                <img src={g5} alt="v1button" className={classes.imgbut} />
              </button>
              <button
                className={classes.buttonitem}
                onClick={this.changeColor(v6)}
              >
                <img src={g6} alt="v1button" className={classes.imgbut} />
              </button>
              <button
                className={classes.buttonitem}
                onClick={this.changeColor(v7)}
              >
                <img src={g7} alt="v1button" className={classes.imgbut} />
              </button>
              <button
                className={classes.buttonitem}
                onClick={this.changeColor(v8)}
              >
                <img src={g8} alt="v1button" className={classes.imgbut} />
              </button>
              <button
                className={classes.buttonitem}
                onClick={this.changeColor(v9)}
              >
                <img src={g9} alt="v1button" className={classes.imgbut} />
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ThuKinh;
