import React, { Component } from 'react';
import Header from '../Header';
import Carousel from '../Carousel';
import Intro from '../Intro';
import Footer from '../Footer';

class Home extends Component {
    render() {
        return (
            <div>
                <Header />
                <Carousel />
                <Intro />
                <Footer />
            </div>
        );
    }
}

export default Home;